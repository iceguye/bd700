#    Copyright © 2016-2019 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.
#

var ndCanvas = func {

    var font_mapper = func(family,weight) {
        if (family == "'DejaVu Sans Mono'" and weight == "normal") {
            return "DejaVuSansMono.ttf";
        }
    };

    var my_canvas = canvas.new({
        "name": "ND",
        "size": [1024, 1024],
        "view": [1024, 1024],
        "mipmapping": 1
    });

    var nd = my_canvas.createGroup();

    canvas.parsesvg(
        nd,
        "Aircraft/bd700/Models/nd/ND.svg",
        {'font-mapper': font_mapper}
    );

    var nd_wpt = my_canvas.createGroup();
    nd_wpt.set("clip", "rect(114.38px, 843.00px, 670.00px, 198.00px)"); 
    nd_wpt.setCenter(519.02, 566.39);
    var wpt_symbols = [];
    var wpt_id = [];
    var wpt_line = [];
    var map_move = 0.0;

    var mdf_rte = func {
        nd_wpt.removeAllChildren();
        var wpt_num = bd700.waypoints.size();
        for (i=0; i<wpt_num; i=i+1){
            append(wpt_symbols, 0);
            append(wpt_id, 0);
            append(wpt_line, 0);
            wpt_symbols[i] = nd_wpt.createChild("group");
            wpt_id[i] =  nd_wpt.createChild("text");
            wpt_line[i] = nd_wpt.createChild("path");
            canvas.parsesvg(wpt_symbols[i], "Aircraft/bd700/Models/nd/wpt.svg");
        }          
    }
    setlistener("/instrumentation/fmz/waypoints-num", mdf_rte);

    var compass = nd.getElementById("compass");
    var hdg = nd.getElementById("hdg");
    var staticAirTemp = nd.getElementById("staticAirTemp");
    var totalAirTemp = nd.getElementById("totalAirTemp");
    var groundSpd = nd.getElementById("groundSpd");
    var trueSpd = nd.getElementById("trueSpd");
    var aplSymMapCtr = nd.getElementById("aplSymMapCtr");

    compass.set("clip", "rect(114.38, 843.00, 332.00, 198.00)");

 
    var nd_fast_loop = maketimer(0.04, func() {
        var hdg_mag = getprop("orientation/heading-magnetic-deg");
        compass.setRotation(-hdg_mag*D2R);
        hdg.setText(sprintf("%03d", hdg_mag));
    });
    nd_fast_loop.start();
    
    var nd_slow_loop = maketimer(0.25, func() {
        var mach = getprop("velocities/mach");
        var sat_degc = getprop("environment/temperature-degc");
        var sat_kelvin = sat_degc + 273.15;
        var tat_kelvin = sat_kelvin * (1+((1.4-1)*math.pow(mach,2)/2));
        var tat_degc = tat_kelvin - 273.15;
        var groundspeed_kt = getprop("velocities/groundspeed-kt");
        var hdg_true = getprop("orientation/heading-deg");
        var wind_hdg = getprop("environment/wind-from-heading-deg");
        var wind_spd_kt = getprop("environment/wind-speed-kt");
        var headwind_kt = math.cos(math.abs((wind_hdg-hdg_true)*D2R))*wind_spd_kt;
        var truespeed_kt = groundspeed_kt + headwind_kt;

        if (truespeed_kt < 0) {
            truespeed_kt = 0.0;
        }

        staticAirTemp.setText(sprintf("%02d", sat_degc));
        totalAirTemp.setText(sprintf("%02d", tat_degc));
        groundSpd.setText(sprintf("%3d", groundspeed_kt));
        trueSpd.setText(sprintf("%3d", truespeed_kt));
    });
    nd_slow_loop.start();

    var moving_rte = maketimer(0.1, func(){
        var wpt_y = [];
        var wpt_x = [];
        var map_orig_x = 519.25-(47.21/2);
        var map_orig_y = 1024-567.75-(47.57/2);
        var ert_R = 6371000;
        var crt_lat = getprop("/position/latitude-deg");
        var crt_lon = getprop("/position/longitude-deg");
        var hdg_tru = getprop("/orientation/heading-deg");
        var wpt_num = bd700.waypoints.size();
        for (i=0; i<wpt_num; i=i+1){
            var crt_lat_rad = crt_lat * D2R;
            var crt_lon_rad = crt_lon * D2R;

            var wpt_lat = bd700.waypoints.vector[i].lat_nd;
            var wpt_lon = bd700.waypoints.vector[i].lon_nd;
            var wpt_id_text = bd700.waypoints.vector[i].id;
            var wpt_lat_rad = wpt_lat * D2R;
            var wpt_lon_rad = wpt_lon * D2R;
            var delta_lat = wpt_lat_rad - crt_lat_rad;
            var delta_lon = wpt_lon_rad - crt_lon_rad;
            var a_cfn = math.sin(delta_lat/2) * math.sin(delta_lat/2) + 
                        math.cos(crt_lat_rad) * math.cos(wpt_lat_rad) *
                        math.sin(delta_lon/2) * math.sin(delta_lon/2);
            var c_cfn = 2 * math.atan2(math.sqrt(a_cfn), math.sqrt(1-a_cfn));
            var wpt_dst = ert_R * c_cfn / 1000 / 1.852;
            var wpt_dst_px = wpt_dst * (8.6975);
            var y_cfn = math.sin(delta_lon) * math.cos(wpt_lat_rad);
            var x_cfn = math.cos(crt_lat_rad) * math.sin(wpt_lat_rad) -
                        math.sin(crt_lat_rad) * math.cos(wpt_lat_rad) *
                        math.cos(delta_lon);
            var brg = math.atan2(y_cfn, x_cfn) * R2D;
            brg = math.fmod((brg + 360), 360);
            var delta_brg = (brg - hdg_tru) * D2R;
            append(wpt_y, map_orig_y - wpt_dst_px * math.cos(delta_brg));
            append(wpt_x, map_orig_x + wpt_dst_px * math.sin(delta_brg));

            if (i>0) {
                var prev_wpt_type = bd700.waypoints.vector[(i-1)].type;
                if (prev_wpt_type != "Vectors") {
                    wpt_line[i].reset();
                    wpt_line[i].setStrokeLineWidth(4);
                    wpt_line[i].set("stroke", "rgba(255,255,255,255)");
                    wpt_line[i].moveTo(wpt_x[i-1]+(47.21/2), wpt_y[i-1]+(47.57/2));
                    wpt_line[i].lineTo(wpt_x[i]+(47.21/2), wpt_y[i]+(47.57/2));
                }
            }
            wpt_symbols[i].setTranslation(wpt_x[i], wpt_y[i]);
            wpt_id[i].setTranslation(wpt_x[i]+46, wpt_y[i]+20);
            wpt_id[i].setText(sprintf("%s", wpt_id_text));
            wpt_id[i].setScale(0.6);
            wpt_id[i].setAlignment("left-center");
        }
    });
    moving_rte.start();

    my_canvas.addPlacement({"node": "ND-Screen"});
}

setlistener("sim/signals/fdm-initialized", ndCanvas);
