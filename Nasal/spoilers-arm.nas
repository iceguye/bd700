#    Copyright © 2016-2019 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.
#

props.globals.initNode("fdm/jsbsim/fcs/ground-spoilers-cmd-norm", 0.0);
props.globals.initNode("fdm/jsbsim/fcs/spoilers-armed-set", 0.0);
props.globals.initNode("gear/gear[0]/wow", 0.0);
props.globals.initNode("gear/gear[1]/wow", 0.0);

var spoilersArm = func {
    var in_air = 0.0;

    var spoilers_arm_loop = maketimer(1.5, func() {
        var spoilers_armed_set = getprop("fdm/jsbsim/fcs/spoilers-armed-set");
        var wow = (getprop("gear/gear[1]/wow") or getprop("gear/gear[2]/wow"));
        if (wow == 1.0 and spoilers_armed_set == 1.0 and in_air == 1.0) {
            setprop("fdm/jsbsim/fcs/ground-spoilers-cmd-norm", 1.0);
        }
        if (spoilers_armed_set == 0.0) {
            setprop("fdm/jsbsim/fcs/ground-spoilers-cmd-norm", 0.0);
        }
        if (wow == 1.0) {
            in_air = 0.0;
        } else {
            in_air = 1.0;
        }
    });
    spoilers_arm_loop.start();
    

}
setlistener("/sim/signals/fdm-initialized", spoilersArm);

