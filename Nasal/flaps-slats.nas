#    Copyright © 2016-2019 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.
#

props.globals.initNode("fdm/jsbsim/fcs/flap-cmd-bd700", 0.0);
props.globals.initNode("fdm/jsbsim/fcs/slat-cmd-bd700", 0.0);

var flaps_slats = func {

    var flap_cmd_norm = getprop("fdm/jsbsim/fcs/flap-cmd-norm");

    if (flap_cmd_norm == 0.0) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.0);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 0.0);
    } else if (flap_cmd_norm == 0.2) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.0);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    } else if (flap_cmd_norm == 0.4) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.2);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    } else if (flap_cmd_norm == 0.8) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 0.5333333333333333);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    } else if (flap_cmd_norm == 1.000) {
        setprop("fdm/jsbsim/fcs/flap-cmd-bd700", 1.0);
        setprop("fdm/jsbsim/fcs/slat-cmd-bd700", 1.0);
    }
}
setlistener("/controls/flight/flaps", flaps_slats);