
DESCRIPTION
===========

    This program is a FlightGear aircraft model of BD700. This model
    can fly more than 6000 nautical miles with 0.8 mach. It can also
    takeoff and land on a short runway which is less than 900 meters
    with an airspeed of 108 knots.

SYSTEM REQUIREMENT
==================

    This program is tested on FlightGear Version 2017.4.0, so it works
    correctly on this version, and it may work okay in later versions.
    
INSTALLATION
============

    1) Copy the the entire folder of this program into your aircraft
       folder of FlightGear.

    2) Change (or make sure) your this aircraft's folder name is
       exactly "bd700".

DEVELOPMENT
===========

    Any contribution for this aircraft is deeply welcome.

    If you like to use git, you can send merge request, or you can
    contact IceGuye to get a pushing access to the original git
    repository.

    If you do not like to use git. You can directly send your works to
    IceGuye by email.

    In any way you contributing to the program, your name (or nickname
    as you wish) will be added to the AUTHORS part of this README
    file.

AUTHORS
=======

    IceGuye, lanbo64, 梦想客机, 雷神伊尔, Sidi Liang, and the FSCTR Community.

CONTACTS
========

    If you have any concern about this project or flight simulation,
    please do not hesitate to contact via:

    Email: Ice.Guye@bk.ru

    IceGuye is a pilot and flight instructor in real world. However,
    this project is not for real flight training. She will never
    answer anything about real flight, nor about your school
    homework. If you have this kind of questions, please consult your
    flight instructor or your school supervisor.

COPYRIGHT
=========

    Copyright © 2016-2019 IceGuye.

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, version 3 of the
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <http://www.gnu.org/licenses/>.

说明
===

    本软件是一个运行在FlightGear中的BD700型飞机模型。这个模型可以以0.8
    马赫的速度飞行超过6000海里的距离。它还能以108节的速度从不到900米长
    的短跑道上起飞或降落。

系统要求
=======

    这个软件测试于FlightGear 2017.4.0版上，因此它可以在此版本上正确地运
    行，并且它可能可以运行在以后的版本上。

安装
====

    1、复制这个程序的整个文件夹到你的FlightGear的飞机文件夹中。

    2、修改（或者确保）此飞机的文件夹名称为精确的“bd700”这几个字。

开发
====

    对本飞机模型的任何贡献都是深深地欢迎。

    如果你喜欢用git，你可以发送merge request，或者，你也可以联系姑射冰
    尘，让她授予你原git仓库的读写权限。

    如果你不喜欢用git，你可以直接将你的贡献作品通过电子邮件发送给姑射
    冰尘。

    无论你使用何种方式贡献，你的名字（或者你的昵称）都会被写入到“作者”
    一栏中。

作者
====

    姑射冰尘，lanbo64，雷神伊尔，梦想客机，Sidi Liang，以及整个模拟飞行社区。

联系
====

    如果你有任何关于本项目或者模拟飞行的问题，请随时通过以下方式联系我
    们：

    Email: Ice.Guye@bk.ru

    姑射冰尘是一名现实中的飞行员和飞行教员，但是本项目不可用于真实飞行。
    姑射冰尘不会回答任何关于真实飞行或者课题作业相关的疑问。如果你有任
    何关于这方面的问题，请咨询你的飞行教员或课题辅导员。

版权
====

    版权所有 © 2016-2019 姑射冰尘。

    本程序是自由软件：你可以在自由软件基金会出版的GNU通用公共许可证第3
    版地约束之下，再发行和/或修改本软件。

    这个软件只是在它将会有用这样希望之下被传播，但【没有任何的保障】；
    也【没有暗示对任何目的的适用性和适销性】。具体请参考GNU通用公共许
    可证。

    你应该在本软件中收到一份GNU通用公共许可证的副本。如果没有，请访问
    网页：http://www.gnu.org/licenses/。